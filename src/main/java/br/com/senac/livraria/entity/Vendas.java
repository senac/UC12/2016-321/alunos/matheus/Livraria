
package br.com.senac.livraria.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class Vendas implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date dataVendas ;
    
    @Column(nullable = true)
    private float total ;
    
    @ManyToOne
    @JoinColumn(name = "idCliente")
    private Cliente cliente ;
    
    @Transient
    private List<ItemVendas> itens ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataVendas() {
        return dataVendas;
    }

    public void setDataVendas(Date dataVendas) {
        this.dataVendas = dataVendas;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemVendas> getItens() {
        return itens;
    }

    public void setItens(List<ItemVendas> itens) {
        this.itens = itens;
    }
    
    
    
    
}
