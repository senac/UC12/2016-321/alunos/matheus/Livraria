package br.com.senac.livraria.banco;

import br.com.senac.livraria.entity.Genero;

public class GeneroDAO extends DAO<Genero> {

    public GeneroDAO() {
        super(Genero.class);
    }

}
