package br.com.senac.livraria.banco;

import br.com.senac.livraria.entity.Cliente;

public class ClienteDAO extends DAO<Cliente> {

    public ClienteDAO() {
        super(Cliente.class);
    }

}
