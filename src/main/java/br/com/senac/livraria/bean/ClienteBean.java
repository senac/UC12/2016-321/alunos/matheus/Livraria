package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.ClienteDAO;
import br.com.senac.livraria.entity.Cliente;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "ClienteBean")
@RequestScoped
public class ClienteBean extends Bean {

    private Cliente cliente;
    private ClienteDAO dao;

    public ClienteBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new ClienteDAO();
        this.cliente = new Cliente();
    }

    public String getCodigo() {
        return this.cliente.getId() == 0 ? "" : String.valueOf(this.cliente.getId());
    }

    public void novo() {
        this.cliente = new Cliente();
    }

    public void salvar() {

        try {

            if (this.cliente.getId() == 0) {
                dao.save(cliente);
                addMessageInfo("Salvo com sucesso!");
                novo();
            } else {
                dao.update(cliente);
                addMessageInfo("Alterado com sucesso!");
                novo();
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Cliente cliente) {
        try {
            dao.delete(cliente.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setAutor(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getLista() {
        return this.dao.findAll();
    }
}
