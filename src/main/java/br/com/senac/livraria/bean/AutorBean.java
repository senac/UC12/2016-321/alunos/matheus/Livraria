package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.AutorDAO;
import br.com.senac.livraria.entity.Autor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "autorBean")
@RequestScoped
public class AutorBean extends Bean {

    private Autor autor;
    private AutorDAO dao;

    public AutorBean() {
    }

    @PostConstruct
    public void init() {
        this.autor = new Autor();
        this.dao = new AutorDAO();
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public String getCodigo() {
        return this.autor.getIdAutor() == 0 ? "" : String.valueOf(this.autor.getIdAutor());
    }

    public void salvar() {
        try {
            if (this.autor.getIdAutor() == 0) {
                dao.save(autor);
                addMessageInfo("Salvo com sucesso!");
                novo();
            } else {
                dao.update(autor);
                addMessageInfo("Alterado com sucesso!");
                novo();
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
    }

    public void novo() {
        this.autor = new Autor();
    }

    public List<Autor> getLista() {
        return this.dao.findAll();
    }

    public void excluir(Autor autor) {

        try {
            dao.delete(autor.getIdAutor());
            addMessageInfo("Removido com sucesso!");
            novo();

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

}
